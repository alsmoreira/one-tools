Describle

To use the OpenNebula Cloud API for Java in your Java project, you have to add to the classpath the org.opennebula.client.jar file and the xml-rpc libraries located in the lib directory.

Compilation and Installation

The Java OCA is part of the OpenNebula core distribution, but it is not installed by default. You will find the packaged code in a tar.gz file following this link: http://dev.opennebula.org/packages.

To compile the Java OCA, untar the source, cd to the java directory and use the build script:

$ cd src/oca/java
$ ./build.sh -d
Compiling java files into class files...
Packaging class files in a jar...
Generating javadocs...

This command will compile and package the code in jar/org.opennebula.client.jar, and the javadoc will be created in share/doc/.

You might want to copy the .jar files to a more convenient directory. You could use /usr/lib/one/java/

$ sudo mkdir /usr/lib/one/java/
$ sudo cp jar/* lib/* /usr/lib/one/java/

Pre-defined Placement Policies

The following list describes the predefined policies that can be configured through the sched.conf file.

Packing Policy

    Target: Minimize the number of cluster nodes in use
    Heuristic: Pack the VMs in the cluster nodes to reduce VM fragmentation
    Implementation: Use those nodes with more VMs running first

RANK = RUNNING_VMS

Striping Policy

    Target: Maximize the resources available to VMs in a node
    Heuristic: Spread the VMs in the cluster nodes
    Implementation: Use those nodes with less VMs running first

RANK = "- RUNNING_VMS"

Load-aware Policy

    Target: Maximize the resources available to VMs in a node
    Heuristic: Use those nodes with less load
    Implementation: Use those nodes with more FREECPU first

RANK = FREECPU


